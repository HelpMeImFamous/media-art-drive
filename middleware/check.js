export default function({ store, route, redirect }) {
  if (route.path === '/info' && store.state.chosenObj === null) {
    redirect('/')
  }
}
