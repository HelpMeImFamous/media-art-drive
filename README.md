## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

## Acknowledgements

Actividad subvencionada por el Ministerio de Cultura y Deporte del Gobierno de España con cargo a las Ayudas a la Modernización e Innovación en las Industrias Culturales y Creativas mediante proyectos digitales y tecnológicos, convocatoria 2018 

Project funded by the Spanish Ministry of Culture and Sports, thanks to the Aid to the Modernization and Innovation of Cultural and Creative Industrias by the means of digital and technological projects, 2018

![logo ministerio](http://www.helpmeimfamous.com/images/logotipobn.jpg)
