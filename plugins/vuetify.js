//import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader, needs internet connection
import 'material-design-icons-iconfont/dist/material-design-icons.css'  //needs package installation sudo yarn add material-design-icons-iconfont or npm install material-design-icons-iconfont --save
import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: colors.blue,
    accent: colors.grey,
    secondary: colors.amber,
    info: colors.teal.lighten1,
    warning: colors.amber.base,
    error: colors.deepOrange.accent4,
    success: colors.green.accent3
  }
})
