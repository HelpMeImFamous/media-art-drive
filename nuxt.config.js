const fs = require('fs')
const path = require('path')
const mime = require('mime-types')
const flat = require('flat')
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')
const pkg = require('./package')

const documents = []

const walkSync = dir => {
  const files = []
  fs.readdirSync(dir).forEach(file => {
    if (file !== '.DS_Store') {
      if (fs.statSync(path.join(dir, file)).isDirectory()) {
        const item = {
          id: path.join(dir, file),
          name: file,
          path: path.join(dir, file),
          children: []
        }
        item.children = walkSync(path.join(dir, file))
        files.push(item)
      } else {
        const node = {
          id: path.join(dir, file),
          name: file,
          path: path.join(dir, file),
          type: mime.lookup(path.join(dir, file))
        }
        if (mime.lookup(path.join(dir, file)) === 'application/json') {
          const doc = require(`./${path.join(dir, file)}`)
          documents.push(flat(doc))
        }
        files.push(node)
      }
    }
  })

  fs.writeFile('static/all.json', JSON.stringify(files), 'utf8')
  fs.writeFile('static/documents.json', JSON.stringify(documents), 'utf8')

  return files
}

const list = walkSync('./root')

module.exports = {
  mode: 'universal',

  env: {
    fileList: list,
    documents,
    dirname: __dirname
  },

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: ['~/assets/style/app.styl'],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: ['@/plugins/vuetify'],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    transpile: ['vuetify/lib'],
    plugins: [new VuetifyLoaderPlugin()],
    loaders: {
      stylus: {
        import: ['~assets/style/variables.styl']
      }
    },

    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
