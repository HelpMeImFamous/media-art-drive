import { uniqBy, orderBy } from 'lodash'
import moment from 'moment'

export const state = () => ({
  drawer: false,
  match: [],
  collection: process.env.documents,
  key: null,
  val: null,
  chosenFile: null,
  chosenObj: null,
  idx: null,
  documents: []
})

export const getters = {
  refs: state => {
    return state.match.map(m => m.ref)
  },
  notFound: state => {
    return state.match.length === 0
  },
  results: (state, getters) => {
    return state.collection.filter(c => {
      return getters.refs.includes(c['mainChars.register.reg'])
    })
  },
  folder: (state, getters) => {
    let tmp = null
    try {
      tmp = state.chosenObj['mainChars.register.reg']
    } catch (err) {
      tmp = null
    }
    return tmp
  },
  artists: (state, getters) => {
    const items = uniqBy(
      getters.results.map(r => {
        return {
          title: r['mainChars.artworkInfo.artist'],
          key: 'mainChars.artworkInfo.artist'
        }
      }),
      'title'
    )
    return {
      action: 'person',
      active: true,
      title: 'Artistas',
      items
    }
  },
  collections: (state, getters) => {
    return {
      action: 'group_work',
      active: true,
      title: 'Colecciones',
      items: uniqBy(
        getters.results.map(r => {
          return {
            title: r['mainChars.register.collection'],
            key: 'mainChars.register.collection'
          }
        }),
        'title'
      )
    }
  },
  format: (state, getters) => {
    return {
      action: 'mdi-folder-multiple-image',
      active: true,
      title: 'Formato',
      items: uniqBy(
        getters.results.map(r => {
          return {
            title: r['mainChars.masterInfo.mediaInfo.media'],
            key: 'mainChars.masterInfo.mediaInfo.media'
          }
        }),
        'title'
      )
    }
  },
  items: (state, getters) => {
    return [getters.artists, getters.collections, getters.format]
  },
  selectedMedia: state => {
    if (state.key && state.val) {
      return state.collection.filter(c => c[state.key] === state.val)
    } else {
      return []
    }
  },
  recentMedia: state => {
    return orderBy(
      state.collection,
      c => moment(c['mainChars.register.addDate'], 'YYYY'),
      ['desc']
    )
  }
}

export const mutations = {
  setResults(state, match) {
    state.match = match
  },
  getInfo(state, obj) {
    state.key = obj.key
    state.val = obj.val
  },
  setChosenFile(state, uri) {
    state.chosenFile = uri
  },
  setChosenObj(state, obj) {
    state.chosenObj = obj
  },
  setDrawer(state, flag) {
    state.drawer = flag
  },
  setIdx(state, idx) {
    state.idx = idx
  },
  setDocuments(state, documents) {
    state.documents = documents
  }
}

export const actions = {
  async nuxtServerInit({ commit }, { app }) {
    const documents = await app.$axios.$get('/documents.json')
    commit('setDocuments', documents)
  }
}
